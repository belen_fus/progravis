#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <stdlib.h>

typedef struct {
    char nom[25];
    int edad;
    char tel[15];
    char dir[35];
}Amigo;

/Funciones de Archivo/
void Crear(void);
void Listar(void);
void Agregar(void);
void Modificar(void);
void Borrar(void);
void Encontrar(void);

/Nombre del Archivo a guardar/
char nombre_fichero[] = "agenda.dat";

FILE *abrir_fichero_b(char cadena[], int metodo);

void main(void)
{
    FILE *fp;
    int opc;
    char cad[3];
   
    fp = abrir_fichero_b(nombre_fichero,0);
    if( fp )
    {
        fclose(fp);
    }
    else
    {
        printf( "Error (NO ABIERTO)\n" );
        puts("Presiona cualquier tecla para continuar.... y Crear el Archivo");
        fp = abrir_fichero_b(nombre_fichero,1);
        fclose(fp);
        getch();    }
   
    do{
    system("cls");
        puts("1. Crear Archivo");
        puts("2. Agregar\n3. Eliminar\n4. Listar\n5. Buscar\n6. Modificar\n7. Salir");
        puts("?Introduce tu opcion y despues enter.");
       
        opc = atoi(gets(cad));
        while(opc<0 || opc>7) /Limitando la entrada de las opciones/   
            opc = atoi(gets(cad));
        system("cls");   
        switch(opc)
        {
        case 1:
                Crear();
                break;
        case 2:   
                Agregar();
                break;
        case 3:
                Borrar();
                break;
        case 4:
                Listar();
                break;
        case 5:
                Encontrar();
                break;
        case 6:
                 Modificar();
                break;
           
        }
       
        /Solo parar cuando sea una de las opciones/
        if(opc<7)
        {
            puts("\n\nPresiona Cualquier Tecla para Regresar al Menu");
            getch();
        }
    }
    while(opc!=7);
   
}