#include <iostream>
#include <windows.h>
#include <conio.h>
#include <String>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ARRIBA 72
#define ABAJO 80
#define ENTER 13
#define V_SIZE 10 //macro de insercion

void dificultad();
using namespace std;

int dif=0,dif2=0; //Declaracion de Sacudida
int i = 0 , N, izq = 1 , der = N-1 , k = N-1 , aux = 0; //Definiciones de Sacudida
int *A = NULL;  //Declaracion de Sacudida
string flecha="->"; //Declaracion movimiento de flecha

void gotoxy(int x,int y)
{
    HANDLE hCon=GetStdHandle(STD_OUTPUT_HANDLE);
    COORD dwPos;
    dwPos.X=x;
    dwPos.Y=y;
    SetConsoleCursorPosition(hCon, dwPos);
 }

 void pintarmenu()
{

    string titulo= "BIENVENIDO A LOS METODOS DE ORDENAMIENTO";
    string OpSelect[]={"Burbuja","Sacudida","Insercion","Seleccion","Rapida","Mezclas"," Salir"};

 //imprimiendo el menu
    gotoxy(5,0); cout<<titulo;

    for(int i=0; i<7; i++) //Si se aumentas lineas se debe cambiar el limite de i (tambien cambiar TODOS los numeros identicos por el nuevo
    {
        gotoxy(3,2+i); cout<<OpSelect[i];
    }
}

void pintarmenu2()
{
    string OpSelect[]={"Vector","Matriz","Estructura"," Regresar"};

 //imprimiendo el menu

    for(int i=0; i<4; i++) //Si se aumentas lineas se debe cambiar el limite de i (tambien cambiar TODOS los numeros identicos por el nuevo
    {
        gotoxy(3,2+i); cout<<OpSelect[i];
    }
}

//imprimir movimiento de la flecha
int moverse()
{
    gotoxy(1,2+dif); cout<<flecha;

    if(kbhit())
    {
        char tecla=getch();

        if(tecla==ABAJO)
        {
            dif++;

            if(dif<7) //Este maneja a donde vuelve a aparecer la flecha (osea en la primer linea)
            {
                gotoxy(1, 2+dif-1); cout<<"  ";
            }
            else
            {
                gotoxy(1, 2+dif-1); cout<<"  ";
                dif=0;
            }
        }
    if(tecla==ARRIBA)
    {
        dif--;

        if(dif<0)
        {
            gotoxy(1, 2+dif+1); cout<<"  ";
            dif=6; //Este maneja a donde vuelve a aparecer la flecha (osea que aparezca en la ultima linea)
        }
        else
        {
        gotoxy(1, 2+dif+1); cout<<"  ";
        }
    }
   if(tecla==ENTER)
   return dif;
   }
 Sleep(50);
 moverse();
}

int moverse2()
{
    gotoxy(1,2+dif2); cout<<flecha;

    if(kbhit())
    {
        char tecla=getch();

        if(tecla==ABAJO)
        {
            dif2++;

            if(dif2<4) //Este maneja a donde vuelve a aparecer la flecha (osea en la primer linea)
            {
                gotoxy(1, 2+dif2-1); cout<<"  ";
            }
            else
            {
                gotoxy(1, 2+dif2-1); cout<<"  ";
                dif2=0;
            }
        }
    if(tecla==ARRIBA)
    {
        dif2--;

        if(dif2<0)
        {
            gotoxy(1, 2+dif2+1); cout<<"  ";
            dif2=3; //Este maneja a donde vuelve a aparecer la flecha (osea que aparezca en la ultima linea)
        }
        else
        {
        gotoxy(1, 2+dif2+1); cout<<"  ";
        }
    }
   if(tecla==ENTER)
   return dif2;
   }
 Sleep(50);
 moverse2();
}

void casos_burbuja()
{
    pintarmenu2();
    moverse2();
    system("cls");

    switch(dif2)
                {
                    case 0:
                        cout<<"Selecciono Modo Vector\n\nIngrese Sus Datos a Ordenar\n";
                        int array[10];
                        int i,j,aux,a,lim;

                        printf("Digite La Cantidad de Valores Que Desee Ordenar: ");
                        scanf("%i",&lim);
                        //LLenado de vector
                        printf("\n");
                        for(i=0;i<lim;i++)
                        {
                            printf("Valor en [%i]: ",i+1);
                            scanf("%i",&array[i]);
                        }
                        //Ordenamiento
                        for(i=0;i<lim;i++)
                        {
                            for(j=0;j<lim;j++)
                            {
                                if(array[j] > array[j+1])
                                {
                                    aux = array[j];
                                    array[j] = array[j+1];
                                    array[j+1] = aux;
                                }}}

                        printf("\n");
                        //Ascendente
                        for(i=0;i<lim;i++)
                        {
                            printf("%i ",array[i]);
                        }
                        printf("\n\n");

                        //Desendente
                        for(i=lim-1;i>=0;i--)
                        {
                            printf("%i ",array[i]);
                        }
                        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("\tSelecciono Metodo Burbuja");
                        casos_burbuja();
                        break;

                    case 1:
                        cout<<"Selecciono Modo Matriz\n\nIngrese Sus Datos a Ordenar:\n";
                        int f,c,mayor;
                        int matriz[10][10];

                        printf("Cual es el numero de filas y columnas de su matriz?: ");
                        scanf("%i",&lim);
                        printf("\n");
                        //Llenado de matriz
                        for(i=0;i<lim;i++)
                        {
                            for(j=0;j<lim;j++)
                            {
                                printf("Valor en [%i][%i]: ",i+1,j+1);
                                scanf("%i,%i",&matriz[i][j]);
                            }
                        }

                        //ordeno la matriz de mayor a menor
                        for(int i=0; i<lim; i++)
                        {
                            for(int j=0; j<lim; j++)
                            {
                                for(int x=0; x<lim;x++)
                                {
                                    for(int y=0; y<lim; y++)
                                    {
                                        if(matriz[i][j]>matriz[x][y])
                                        {
                                            mayor=matriz[i][j];
                                            matriz[i][j]=matriz[x][y];
                                            matriz[x][y]=mayor;
                                        }}}}}
                    printf("\n");
                    //imprimo la matriz como ordenada
                    for(int i=0; i<lim; i++)
                    {
                        for(int j=0; j<lim; j++)
                        {
                            printf("%i ",matriz[i][j]);
                        }
                                printf("\n");
                    }
                    printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("\tSelecciono Metodo Burbuja");
                        casos_burbuja();
                        break;

                    case 2:
                        cout<<"Selecciono Modo Estructura\n\nEsta Funcion Sera Agregada Proximamente...";
                        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("\tSelecciono Metodo Burbuja");
                        casos_burbuja();
                        break;

                    case 3:
                        dificultad();
                        break;
                }
}

void casos_sacudida()
{
    pintarmenu2();
    moverse2();
    system("cls");

    switch(dif2)
    {
    case 0:
        cout<<"Selecciono Metodo Sacudida en Vector\n";

        A = (int*)malloc(sizeof(int));

        int opcion;

        cout<<endl<<"  >> INGRESE LOS VALORES :  ";
        cout<<endl<<"      -------------------> ";
        cin>>N;

        for( int i = 0 ; i < N ; i++ )
        {
            cout<<endl<<"    - Ingrese el valor ["<<i + 1<<"] = ";
            cin>>A[i];
        }
        //Ordenamiento
        while( der >= izq )
        {
            for( int i = der ; i>= izq ; i-- )
                if( A[i-1] > A[i])
                {
                    aux = A[i-1];
                    A[i-1]=A[i];
                    A[i]=aux;
                    k=i;
              }
      izq = k + 1;
      for( int i = izq ; i <= der ; i++)
            if( A[i-1] > A[i] )
              {
                aux = A[i-1];
                A[i-1]=A[i];
                A[i]=aux;
                k=i;
              }
      der = k-1;
    }
        //Impresion
                cout<<endl<<"  - [ METODO MEDIANTE SHAKER (SACUDIDA) ] - "<<endl;

    for( int i = 0 ; i < N ; i++ )
    {
    cout<<endl<<"      - Valor ["<<i + 1<<"] = "<<A[i]<<endl;
    }
    cout<<endl<<endl;

                        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("\tSelecciono Metodo Sacudida");
                        casos_sacudida();
        break;

    case 1:
        cout<<"Selecciono Metodo Sacudida en Matriz\n";

  A = (int*)malloc(sizeof(int));

  int m[10][10], lim, j;

  cout<<"Cual es el numero de numero de filas y columnas de su matriz?: ";
  cin>>lim;
  //Llenado de matriz
  for( int i = 0 ; i < lim ; i++ )
    {
        for( j=0; j<lim; j++)
        {
            printf("Valor en [%i][%i]: ",i+1,j+1);
            scanf("%i,%i",&m[i][j]);
        }
    }
    //Reasignaciones
    A[0]=m[0][0];
    A[1]=m[0][1];
    A[2]=m[1][0];
    A[3]=m[1][1];
    A[4]=m[1][2];
    A[5]=m[2][0];
    A[6]=m[2][1];
    A[7]=m[2][2];
    A[8]=m[3][0];
    A[9]=m[3][1];
    A[10]=m[3][2];
    A[11]=m[3][3];
    A[12]=m[4][0];
    A[13]=m[4][1];
    A[14]=m[4][2];
    A[15]=m[4][3];
    A[16]=m[4][4];

    k = lim*2-1;  der = lim*2-1; izq = 1;
    //Ordenamiento
    while( der >= izq )
    {
        for( int i = der ; i>= izq ; i-- )
            if( A[i-1] > A[i])
              {
                aux = A[i-1];
                A[i-1]=A[i];
                A[i]=aux;
                k=i;
              }
      izq = k + 1;
      for(int i = izq ; i <= der ; i++)

            if( A[i-1] > A[i] )

              {
                aux = A[i-1];
                A[i-1]=A[i];
                A[i]=aux;
                k=i;
              }
      der = k-1;
    }
    printf("\n");
    //Impresion
    for(int i=0;i<lim*2;i++)
    {
        printf("%i ",A[i]);
    }

    printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("\tSelecciono Metodo Sacudida");
                        casos_sacudida();

        break;

    case 2:
        cout<<"Selecciono Metodo Sacudida en Estructura\n\nEsta funcion sera agregada proximamente...";
        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("\tSelecciono Metodo Sacudida");
                        casos_sacudida();
        break;

    case 3:
        dificultad();
        break;
    }
}

void casos_insercion()
{
    pintarmenu2();
    moverse2();
    system("cls");

    switch(dif2)
    {
    case 0:
        cout<<"\tSelecciono Metodo Insercion"<<endl<<endl;
        #define V_SIZE 10

        int cont, temp, j, argc;
        char *argv[20];
        int v[V_SIZE];

        printf("############ Ordenando por Inserci�n ##########\n\n");
        int size;
        int i; size=10;
        //Lo de abajo genera numeros aleatorios
        cont = cont + 1;
        srand(time(NULL)*cont);
        for(i=0; i<size; i++)
        v[i]=rand()%100;

        //Impresion de estos numeros aleatorios
        printf("No. Aleatorios: ");

        printf("[ ");
        for(i=0; i<size; i++)
        printf("%d ", v[i]);
        printf("]\n");

        printf("N�meros ordenados: ");
        //Ordenamiento
        for(i=0; i<size; i++)
        {
            temp=v[i];
            j=i-1;
        while(j>=0 && v[j] >temp)
        {
            v[j+1] = v[j];
            j--;
        }
        v[j+1] = temp;
        }
        //Impresion de los numeros ordenados
        printf("[ ");
        for(i=0; i<size; i++)
        printf("%d ", v[i]);
        printf("]\n");

        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("\tSelecciono Metodo Insercion");
                        casos_insercion();
        break;

    case 1:
        cout<<"Selecciono Metodo Insercion en Matriz"<<endl;

        int v2[V_SIZE], v3[V_SIZE];

 printf("############ Ordenando por Insercion ##########\n\n");

 int cont2, cont3;
//Generador de numeros aleatorios de la primera fila que simula una matriz
 cont = cont + 1;
 srand(time(NULL)*cont);
 for(i=0; i<size; i++)
 v[i]=rand()%100;
//Generador de numeros aleatorios de la segunda fila que simula una matriz
 cont2 = cont2 + 2;
 srand(time(NULL)*cont2);
 for(i=0; i<size; i++)
 v2[i]=rand()%100;
//Generador de numeros aleatorios de la tercera fila que simula una matriz
 cont3 = cont3 + 3;
 srand(time(NULL)*cont3);
 for(i=0; i<size; i++)
 v3[i]=rand()%100;
//Impresion de los numeros aleatorios de la primera fila
 printf("No. Aleatorios: ");

 printf("[ ");
 for(i=0; i<size; i++)
 printf("%d ", v[i]);
 printf("]\n");

printf("\t\t");
//Impresion de los numeros aleatorios de la segunda fila
  printf("[ ");
 for(i=0; i<size; i++)
 printf("%d ", v2[i]);
 printf("]\n");
 //Impresion de los numeros aleatorios de la tercera fila
 printf("\t\t");
 printf("[ ");
 for(i=0; i<size; i++)
 printf("%d ", v3[i]);
 printf("]\n\n");

//ordenamiento de la primera fila de los numeros ordenados
printf("Numeros ordenados: ");

for(i=0; i<size; i++)
 {
 temp=v[i];
 j=i-1;
 while(j>=0 && v[j] >temp)
 {
 v[j+1] = v[j];
 j--;
 }

 v[j+1] = temp;
 }
//Ordenamiento de la segunda fila de los numeros ordenados
 for(i=0; i<size; i++)
 {
 temp=v2[i];
 j=i-1;
 while(j>=0 && v2[j] >temp)
 {
 v2[j+1] = v2[j];
 j--;
 }

 v2[j+1] = temp;
 }
//Ordenamiento de la tercera fila de los numeros ordenados
 for(i=0; i<size; i++)
 {
 temp=v3[i];
 j=i-1;
 while(j>=0 && v3[j] >temp)
 {
 v3[j+1] = v3[j];
 j--;
 }

 v3[j+1] = temp;
 }

 //Impresion de todo el desmadre
printf("[ ");
 for(i=0; i<size; i++)
 printf("%d ", v[i]);
 printf("]\n");

    printf("\t \t ");

 printf("  [ ");
 for(i=0; i<size; i++)
 printf("%d ", v2[i]);
 printf("]\n");

printf("\t\t");

 printf("   [ ");
 for(i=0; i<size; i++)
 printf("%d ", v3[i]);
 printf("]\n");

                        system("pause");
                        system("cls");
                        printf("\tSelecciono Metodo Insercion");
                        casos_insercion();
        break;

    case 2:
        cout<<"Selecciono Metodo Insercion en Estructura"<<endl<<"Esta funcion sera agreada proximamente...";
        system("pause");
                        system("cls");
                        printf("\tSelecciono Metodo Insercion");
                        casos_insercion();
        break;

    case 3:
        dificultad();
    }
}

void casos_seleccion()
{
    pintarmenu2();
    moverse2();
    system("cls");

    switch(dif2)
    {
    case 0:

    break;
    }
}

void dificultad()
{
    pintarmenu();
    moverse();
    system("cls");

    switch(dif)
    {
        case 0:
            cout<<"Selecciono Metodo Burbuja";
            casos_burbuja();
            break;

        case 1:
            cout<<"Selecciono Metodo Sacudida";
            casos_sacudida();
            break;

        case 2:
            cout<<"Selecciono Metodo Insecion";
            casos_insercion();
            break;

        case 3:
            cout<<"Selecciono Metodo Seleccion";
            casos_seleccion();
            break;

        case 4:
            cout<<"Selecciono Metodo Rapido";
            break;

        case 5:
            cout<<"Selecciono Metodo Mezclas";
            break;

        case 6:
            cout<<"Adios. Perra";
    }
}

int main(){
    system("COLOR F4");
    dificultad();

}

