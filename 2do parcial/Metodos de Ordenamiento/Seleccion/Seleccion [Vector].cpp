#include<conio.h>
#include<stdio.h>

int main(){
    int vect[10];
    int aux, p, lim;

    printf("Cuantos Valores Desea Ordenar?: ");
    scanf("%i",&lim);
    printf("\n");
    for(int i=0; i<lim; i++)
    {
        printf("Valor en [%i]: ",i+1);
        scanf("%i",&vect[i]);
    }

    //Ordenamiento
    for(int i=0; i<lim; i++)
    {
        aux=vect[i];
        p=i;

        for(int j=i+1; j<lim; j++)
        {
            if(vect[j]<aux)
            {
                aux=vect[j];
                p=j;
            }
        }
        vect[p]=vect[i];
        vect[i]=aux;
    }
    printf("\n");
    //Imprimir ordenamiento
    for(int i=0; i<lim; i++)
    {
        printf("%i",vect[i]);
    }
}
