#include<stdio.h>
#include<conio.h>

struct datos
{
	char nombre[15];
	float peso;
};

void nombre(struct datos pers[10],struct datos aux,int limite);
void peso(struct datos pers[10],struct datos aux,int limite);
void imprimir(struct datos pers[10],int lim);

int main(){

    struct datos persona[10];
	struct datos aux;
	int lim,i,op;

	printf("Cantidad de datos a ingresar: ");
	scanf("%d",&lim);
	getchar();
    printf("\n");
	for(i=0;i<lim;i++)
	{
		printf("Nombre %d : ",i+1);
		gets(persona[i].nombre);
		printf("Peso %d : ",i+1);
		scanf("%f",&persona[i].peso);
		getchar();
		printf("\n");
	}

	printf("Opciones por ordenar:\n1.xNombre\n2.xPeso\n--> ");
	scanf("%d",&op);

	if(op==1)
	  nombre(persona,aux,lim);
	else
	if(op==2)
	  peso(persona,aux,lim);
	else
	printf("Error..... ");
}

void nombre(struct datos pers[10],struct datos aux,int limite)
{
    int i,j;
	for(i=0;i<limite;i++)
	  for(j=0;j<(limite-1);j++)
	  {
	  	if(strcmp(pers[j].nombre,pers[j+1].nombre)>0)
	  	{
	  		aux=pers[j];
	  		pers[j]=pers[j+1];
	  		pers[j+1]=aux;
		  }
	  }

	imprimir(pers,limite);
}

void peso(struct datos pers[10],struct datos aux,int limite)
{
	int i,j;
	for(i=1;i<limite;i++)
	{
		aux=pers[i];
	  for(j=i-1;j>=0 && pers[j].peso>aux.peso;j--)
	  {
	  	pers[j+1]=pers[j];
	  }
	  pers[j+1]=aux;
	}
	imprimir(pers,limite);
}

void imprimir(struct datos pers[10],int lim)
{
    int i;
    printf("\n");
	for(i=0;i<lim;i++)
	{
		printf("%s  ",pers[i].nombre);
		printf("%.2fm  ",pers[i].peso);
		printf("\n");
	}
}

