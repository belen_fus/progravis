#include<stdio.h>
#include<conio.h>

int main(){
	int array[10];
	int i,j,aux,a,lim;

	printf("Digite La Cantidad de Valores Que Desee Ordenar: ");
	scanf("%i",&lim);

	printf("\n");
	for(i=0;i<lim;i++)
	{
        printf("Valor en [%i]: ",i+1);
        scanf("%i",&array[i]);
    }

	for(i=0;i<lim;i++)
	{
		for(j=0;j<lim;j++)
		{
			if(array[j] > array[j+1])
			{
				aux = array[j];
				array[j] = array[j+1];
				array[j+1] = aux;
			}
		}
	}

	printf("\n");
	//Ascendente
	for(i=0;i<lim;i++)
	{
		printf("%i ",array[i]);
	}
	printf("\n\n");

	//Desendente
	for(i=lim-1;i>=0;i--)
	{
		printf("%i ",array[i]);
	}

	getch();
	return 0;
}
