#include <iostream>
#include <windows.h>
#include <conio.h>
#include <String>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ARRIBA 72
#define ABAJO 80
#define ENTER 13
#define V_SIZE 10 //Macro de Insercion
#define TAM 6   //Macro de Rapido
#define TAM2 9  //Macro de Seleccion

void dificultad();
using namespace std;

struct datos
{
	char nombre[15];
	float peso;
};

int dif=0,dif2=0; //Declaracion de Sacudida
int i = 0 , N, izq = 1 , der = N-1 , k = N-1 , aux = 0; //Declaracion de Sacudida
int *A = NULL;  //Declaracion de Sacudida
string flecha="->"; //Declaracion movimiento de flecha
int listas[TAM]={12,10,5,6,1,3};    //Declaracion De Rapido
int listas2[TAM2]={20,5,15,50,8,0,10,40,30};   //Declaracion de Seleccion

void mezclar(int arreglo1[], int n1, int arreglo2[], int n2, int arreglo3[]);   //Prototipo de Mezclas
void mezcla(int vector[], int n);   //Prototipo de Mezclas
void nombre(struct datos pers[10],struct datos aux,int limite); //Prototipo de Estructura
void peso(struct datos pers[10],struct datos aux,int limite);   //Prototipo de Estructura
void imprimir(struct datos pers[10],int lim);   //Prototipo de Estructura

void nombre(struct datos pers[10],struct datos aux,int limite)
{
    int i,j;
	for(i=0;i<limite;i++)
	  for(j=0;j<(limite-1);j++)
	  {
	  	if(strcmp(pers[j].nombre,pers[j+1].nombre)>0)
	  	{
	  		aux=pers[j];
	  		pers[j]=pers[j+1];
	  		pers[j+1]=aux;
		  }
	  }

	imprimir(pers,limite);
}
void peso(struct datos pers[10],struct datos aux,int limite)
{
	int i,j;
	for(i=1;i<limite;i++)
	{
		aux=pers[i];
	  for(j=i-1;j>=0 && pers[j].peso>aux.peso;j--)
	  {
	  	pers[j+1]=pers[j];
	  }
	  pers[j+1]=aux;
	}
	imprimir(pers,limite);
}

void imprimir(struct datos pers[10],int lim)
{
    int i;
    printf("\n");
	for(i=0;i<lim;i++)
	{
	    printf("%s  ",pers[i].nombre);
		printf("%.2fkg  ",pers[i].peso);
		printf("\n");
	}
}

void base()
{
    struct datos persona[10];
	struct datos aux;
	int lim,i,op;

	printf("Cantidad de datos a ingresar: ");
	scanf("%d",&lim);
	getchar();
    printf("\n");
	for(i=0;i<lim;i++)
	{
		printf("Nombre %d : ",i+1);
		gets(persona[i].nombre);
		printf("Peso %d : ",i+1);
		scanf("%f",&persona[i].peso);
		getchar();
		printf("\n");
	}

	printf("Opciones por ordenar:\n1.xNombre\n2.xPeso\n--> ");
	scanf("%d",&op);

	if(op==1)
	  nombre(persona,aux,lim);
	else
	if(op==2)
	  peso(persona,aux,lim);
	else
	printf("Error..... ");
}

int comparacion(const void *i, const void *j){ //Funcion de Rapido
    return *(int *)i - *(int *)j;
}
void mezclar(int arreglo1[], int n1, int arreglo2[], int n2, int arreglo3[])    //Funcion de Mezclas
{
    int x1=0, x2=0, x3=0;

    while (x1 < n1 && x2 < n2)
    {
            if (arreglo1[x1] < arreglo2[x2])
            {
                arreglo3[x3] = arreglo1[x1];
                x1++;
            }
            else
            {
                arreglo3[x3] = arreglo2[x2];
                x2++;
            }
    x3++;
    }

    while (x1 < n1)
    {
        arreglo3[x3] = arreglo1[x1];
        x1++;
        x3++;
    }

    while (x2 < n2)
    {
        arreglo3[x3] = arreglo2[x2];
        x2++;
        x3++;
    }
}

void mezcla(int vector[], int n)    //Funcion de Mezclas
{
    int *vector1, *vector2, n1, n2, x, y;
    if (n > 1)
    {
        if (n % 2 == 0)
            n1 = n2 = (int) n / 2;
        else
        {
            n1 = (int) n / 2; n2 = n1 + 1;
        }

    vector1 = new int [n1];
    vector2 = new int [n2];

        for(x = 0; x < n1; x++)
            vector1[x] = vector[x];
        for(y=0; y < n2; x++, y++)
            vector2[y] = vector[x];

        mezcla(vector1, n1);
        mezcla(vector2, n2);
        mezclar(vector1, n1, vector2, n2, vector);
        delete vector1;
        delete vector2;
    }
}

void gotoxy(int x,int y)
{
    HANDLE hCon=GetStdHandle(STD_OUTPUT_HANDLE);
    COORD dwPos;
    dwPos.X=x;
    dwPos.Y=y;
    SetConsoleCursorPosition(hCon, dwPos);
 }

 void pintarmenu()
{

    string titulo= "**********BIENVENIDO AL MENU DE LOS METODOS DE ORDENAMIENTO**********";
    string OpSelect[]={"Burbuja","Sacudida","Insercion","Seleccion","Rapida","Mezclas"," Salir"};

 //imprimiendo el menu
    gotoxy(5,0); cout<<titulo;

    for(int i=0; i<7; i++) //Si aumentas lineas se debe cambiar el limite de i (tambien cambiar TODOS los numeros identicos por el nuevo
    {
        gotoxy(3,2+i); cout<<OpSelect[i];
    }
}

void pintarmenu2()
{
    string OpSelect[]={"Vector","Matriz","Estructura"," Regresar"};

 //imprimiendo el menu

    for(int i=0; i<4; i++) //Si aumentas lineas se debe cambiar el limite de i (tambien cambiar TODOS los numeros identicos por el nuevo
    {
        gotoxy(3,2+i); cout<<OpSelect[i];
    }
}

int moverse()   //imprimir movimiento de la flecha
{
    gotoxy(1,2+dif); cout<<flecha;

    if(kbhit())
    {
        char tecla=getch();

        if(tecla==ABAJO)
        {
            dif++;

            if(dif<7) //Este maneja a donde vuelve a aparecer la flecha (osea en la primer linea)
            {
                gotoxy(1, 2+dif-1); cout<<"  ";
            }
            else
            {
                gotoxy(1, 2+dif-1); cout<<"  ";
                dif=0;
            }
        }
    if(tecla==ARRIBA)
    {
        dif--;

        if(dif<0)
        {
            gotoxy(1, 2+dif+1); cout<<"  ";
            dif=6; //Este maneja a donde vuelve a aparecer la flecha (osea que aparezca en la ultima linea)
        }
        else
        {
        gotoxy(1, 2+dif+1); cout<<"  ";
        }
    }
   if(tecla==ENTER)
   return dif;
   }
 Sleep(50);
 moverse();
}

int moverse2()
{
    gotoxy(1,2+dif2); cout<<flecha;

    if(kbhit())
    {
        char tecla=getch();

        if(tecla==ABAJO)
        {
            dif2++;

            if(dif2<4) //Este maneja a donde vuelve a aparecer la flecha (osea en la primer linea)
            {
                gotoxy(1, 2+dif2-1); cout<<"  ";
            }
            else
            {
                gotoxy(1, 2+dif2-1); cout<<"  ";
                dif2=0;
            }
        }
    if(tecla==ARRIBA)
    {
        dif2--;

        if(dif2<0)
        {
            gotoxy(1, 2+dif2+1); cout<<"  ";
            dif2=3; //Este maneja a donde vuelve a aparecer la flecha (osea que aparezca en la ultima linea)
        }
        else
        {
        gotoxy(1, 2+dif2+1); cout<<"  ";
        }
    }
   if(tecla==ENTER)
   return dif2;
   }
 Sleep(50);
 moverse2();
}

void casos_burbuja()
{
    pintarmenu2();
    moverse2();
    system("cls");

    switch(dif2)
                {
                    case 0:
                        cout<<"----------Selecciono Modo Vector----------\n\nIngrese Sus Datos a Ordenar\n";
                        int array[10];
                        int i,j,aux,a,lim;

                        printf("Digite La Cantidad de Valores Que Desee Ordenar: ");
                        scanf("%i",&lim);
                        //LLenado de vector
                        printf("\n");
                        for(i=0;i<lim;i++)
                        {
                            printf("Valor en [%i]: ",i+1);
                            scanf("%i",&array[i]);
                        }
                        //Ordenamiento
                        for(i=0;i<lim;i++)
                        {
                            for(j=0;j<lim;j++)
                            {
                                if(array[j] > array[j+1])
                                {
                                    aux = array[j];
                                    array[j] = array[j+1];
                                    array[j+1] = aux;
                                }}}

                        printf("\n");
                        //Ascendente
                        for(i=0;i<lim;i++)
                        {
                            printf("%i ",array[i]);
                        }
                        printf("\n\n");

                        //Desendente
                        for(i=lim-1;i>=0;i--)
                        {
                            printf("%i ",array[i]);
                        }
                        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("----------Selecciono Metodo Burbuja----------");
                        casos_burbuja();
                        break;

                    case 1:
                        cout<<"----------Selecciono Modo Matriz----------\n\nIngrese Sus Datos a Ordenar:\n";
                        int f,c,mayor;
                        int matriz[10][10];

                        printf("Cual es el numero de filas y columnas de su matriz?: ");
                        scanf("%i",&lim);
                        printf("\n");
                        //Llenado de matriz
                        for(i=0;i<lim;i++)
                        {
                            for(j=0;j<lim;j++)
                            {
                                printf("Valor en [%i][%i]: ",i+1,j+1);
                                scanf("%i,%i",&matriz[i][j]);
                            }
                        }

                        //ordeno la matriz de mayor a menor
                        for(int i=0; i<lim; i++)
                        {
                            for(int j=0; j<lim; j++)
                            {
                                for(int x=0; x<lim;x++)
                                {
                                    for(int y=0; y<lim; y++)
                                    {
                                        if(matriz[i][j]>matriz[x][y])
                                        {
                                            mayor=matriz[i][j];
                                            matriz[i][j]=matriz[x][y];
                                            matriz[x][y]=mayor;
                                        }}}}}
                    printf("\n");
                    //imprimo la matriz como ordenada
                    for(int i=0; i<lim; i++)
                    {
                        for(int j=0; j<lim; j++)
                        {
                            printf("%i ",matriz[i][j]);
                        }
                                printf("\n");
                    }
                    printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("----------Selecciono Metodo Burbuja----------");
                        casos_burbuja();
                        break;

                    case 2:
                        cout<<"----------Selecciono Modo Estructura----------\n";
                       base();
                        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("----------Selecciono Metodo Burbuja----------");
                        casos_burbuja();
                        break;

                    case 3:
                        dificultad();
                        break;
                }
}

void casos_sacudida()
{
    pintarmenu2();
    moverse2();
    system("cls");

    switch(dif2)
    {
    case 0:
        cout<<"----------Selecciono Metodo Sacudida en Vector----------"<<endl;

        A = (int*)malloc(sizeof(int));
        int opcion;
        cout<<endl<<"  >> INGRESE CANTIDAD DE VALORES :  ";
        cout<<endl<<"      -------------------> ";
        cin>>N;

        for( int i = 0 ; i < N ; i++ )
        {
            cout<<endl<<"    - Ingrese el valor ["<<i + 1<<"] = ";
            cin>>A[i];
        }
        //Ordenamiento
        while( der >= izq )
        {
            for( int i = der ; i>= izq ; i-- )
                if( A[i-1] > A[i])
                {
                    aux = A[i-1];
                    A[i-1]=A[i];
                    A[i]=aux;
                    k=i;
              }
      izq = k + 1;
      for( int i = izq ; i <= der ; i++)
            if( A[i-1] > A[i] )
              {
                aux = A[i-1];
                A[i-1]=A[i];
                A[i]=aux;
                k=i;
              }
      der = k-1;
    }
        //Impresion
                cout<<endl<<"  - [ METODO MEDIANTE SHAKER (SACUDIDA) ] - "<<endl;

    for( int i = 0 ; i < N ; i++ )
    {
    cout<<endl<<"      - Valor ["<<i + 1<<"] = "<<A[i]<<endl;
    }
    cout<<endl<<endl;

                        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("----------Selecciono Metodo Sacudida----------");
                        casos_sacudida();
        break;

    case 1:
        cout<<"---------Selecciono Metodo Sacudida en Matriz----------"<<endl<<endl;

  A = (int*)malloc(sizeof(int));
  int m[10][10], lim, j;

  cout<<"Cual es el numero de numero de filas y columnas de su matriz?: ";
  cin>>lim;
  //Llenado de matriz
  cout<<endl;
  for( int i = 0 ; i < lim ; i++ )
    {
        for( j=0; j<lim; j++)
        {
            printf("Valor en [%i][%i]: ",i+1,j+1);
            scanf("%i,%i",&m[i][j]);
        }
    }
    //Reasignaciones
    A[0]=m[0][0];
    A[1]=m[0][1];
    A[2]=m[1][0];
    A[3]=m[1][1];
    A[4]=m[1][2];
    A[5]=m[2][0];
    A[6]=m[2][1];
    A[7]=m[2][2];
    A[8]=m[3][0];
    A[9]=m[3][1];
    A[10]=m[3][2];
    A[11]=m[3][3];
    A[12]=m[4][0];
    A[13]=m[4][1];
    A[14]=m[4][2];
    A[15]=m[4][3];
    A[16]=m[4][4];

    k = lim*2-1;  der = lim*2-1; izq = 1;
    //Ordenamiento
    while( der >= izq )
    {
        for( int i = der ; i>= izq ; i-- )
            if( A[i-1] > A[i])
              {
                aux = A[i-1];
                A[i-1]=A[i];
                A[i]=aux;
                k=i;
              }
      izq = k + 1;
      for(int i = izq ; i <= der ; i++)

            if( A[i-1] > A[i] )

              {
                aux = A[i-1];
                A[i-1]=A[i];
                A[i]=aux;
                k=i;
              }
      der = k-1;
    }
    printf("\n");
    //Impresion
    for(int i=0;i<lim*2;i++)
    {
        printf("%i ",A[i]);
    }

                        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("-----------Selecciono Metodo Sacudida----------");
                        casos_sacudida();

        break;

    case 2:
        cout<<"----------Selecciono Metodo Sacudida en Estructura----------\n\n";
        base();
        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("----------Selecciono Metodo Sacudida----------");
                        casos_sacudida();
        break;

    case 3:
        dificultad();
        break;
    }
}

void casos_insercion()
{
    pintarmenu2();
    moverse2();
    system("cls");

    switch(dif2)
    {
    case 0:
        cout<<"----------Selecciono Metodo Insercion----------"<<endl<<endl;
        #define V_SIZE 10

        int cont, temp, j, argc;
        char *argv[20];
        int v[V_SIZE];

        printf("**********Ordenado por Insercion**********\n\n");
        int size;
        int i; size=10;
        //Lo de abajo genera numeros aleatorios
        cont = cont + 1;
        srand(time(NULL)*cont);
        for(i=0; i<size; i++)
            v[i]=rand()%100;

        //Impresion de estos numeros aleatorios
        printf("No. Aleatorios: ");

        printf("[ ");
        for(i=0; i<size; i++)
            printf("%d ", v[i]);
        printf("]\n\n");

        printf("Numeros ordenados: ");
        //Ordenamiento
        for(i=0; i<size; i++)
        {
            temp=v[i];
            j=i-1;
        while(j>=0 && v[j] >temp)
        {
            v[j+1] = v[j];
            j--;
        }
        v[j+1] = temp;
        }
        //Impresion de los numeros ordenados
        printf("[ ");
        for(i=0; i<size; i++)
            printf("%d ", v[i]);
        printf("]\n");

        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("----------Selecciono Metodo Insercion----------");
                        casos_insercion();
        break;

    case 1:
        cout<<"----------Selecciono Metodo Insercion en Matriz----------"<<endl;

        int v2[V_SIZE], v3[V_SIZE];
        printf("\n**********Ordenado por Insercion**********\n\n");

        int cont2, cont3;
        //Generador de numeros aleatorios de la primera fila que simula una matriz
        cont = cont + 1;
        srand(time(NULL)*cont);
        for(i=0; i<size; i++)
            v[i]=rand()%100;
        //Generador de numeros aleatorios de la segunda fila que simula una matriz
        cont2 = cont2 + 2;
        srand(time(NULL)*cont2);
        for(i=0; i<size; i++)
            v2[i]=rand()%100;
        //Generador de numeros aleatorios de la tercera fila que simula una matriz
        cont3 = cont3 + 3;
        srand(time(NULL)*cont3);
        for(i=0; i<size; i++)
            v3[i]=rand()%100;
        //Impresion de los numeros aleatorios de la primera fila
        printf("No. Aleatorios: ");

        printf("[ ");
        for(i=0; i<size; i++)
            printf("%d ", v[i]);
            printf("]\n");

        printf("\t\t");
        //Impresion de los numeros aleatorios de la segunda fila
        printf("[ ");
        for(i=0; i<size; i++)
            printf("%d ", v2[i]);
            printf("]\n");
        //Impresion de los numeros aleatorios de la tercera fila
        printf("\t\t");
        printf("[ ");
        for(i=0; i<size; i++)
            printf("%d ", v3[i]);
            printf("]\n\n");

        //ordenamiento de la primera fila de los numeros ordenados
        printf("Numeros ordenados: ");

        for(i=0; i<size; i++)
        {
            temp=v[i];
            j=i-1;
        while(j>=0 && v[j] >temp)
        {
            v[j+1] = v[j];
            j--;
        }
        v[j+1] = temp;
        }
        //Ordenamiento de la segunda fila de los numeros ordenados
        for(i=0; i<size; i++)
        {
            temp=v2[i];
            j=i-1;
        while(j>=0 && v2[j] >temp)
        {
        v2[j+1] = v2[j];
        j--;
        }
        v2[j+1] = temp;
        }
        //Ordenamiento de la tercera fila de los numeros ordenados
        for(i=0; i<size; i++)
        {
            temp=v3[i];
            j=i-1;
            while(j>=0 && v3[j] >temp)
            {
                v3[j+1] = v3[j];
                j--;
            }
            v3[j+1] = temp;
        }

        //Impresion de todo el desmadre
        printf("[ ");
        for(i=0; i<size; i++)
            printf("%d ", v[i]);
        printf("]\n");
        printf("\t \t ");
        printf("  [ ");
        for(i=0; i<size; i++)
            printf("%d ", v2[i]);
        printf("]\n");
        printf("\t\t");

        printf("   [ ");
        for(i=0; i<size; i++)
            printf("%d ", v3[i]);
        printf("]\n\n");

                        system("pause");
                        system("cls");
                        printf("----------Selecciono Metodo Insercion----------");
                        casos_insercion();
        break;

    case 2:
        cout<<"Selecciono Metodo Insercion en Estructura"<<endl<<endl;
        base();
        system("pause");
                        system("cls");
                        printf("----------Selecciono Metodo Insercion----------");
                        casos_insercion();
        break;

    case 3:
        dificultad();
    }
}

void casos_seleccion()
{
    pintarmenu2();
    moverse2();
    system("cls");

    switch(dif2)
    {
    case 0:
    cout<<"----------Selecciono Metodo Seleccion En Vector----------"<<endl;

    int vect[10];
    int aux, p, lim;

    printf("Cuantos Valores Desea Ordenar?: ");
    scanf("%i",&lim);
    printf("\n");

    for(int i=0; i<lim; i++)
    {
        printf("Valor en [%i]: ",i+1);
        scanf("%i",&vect[i]);
    }

    //Ordenamiento
    for(int i=0; i<lim; i++)
    {
        aux=vect[i];
        p=i;

        for(int j=i+1; j<lim; j++)
        {
            if(vect[j]<aux)
            {
                aux=vect[j];
                p=j;
            }
        }
        vect[p]=vect[i];
        vect[i]=aux;
    }
    printf("\n");
    //Imprimir ordenamiento
    for(int i=0; i<lim; i++)
    {
        printf("%i",vect[i]);
    }

                        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("----------Selecciono Metodo Seleccion----------");
                        casos_seleccion();
    break;

    case 1:
        cout<<"----------Selecciono Metodo Seleccion En Matriz----------"<<endl;

        int temp;   //Declaracion e Inicializacion de un array
        temp=0; //Variable temporal
        int j;  //variables corredoras del ciclo

        printf("\nNumeros Desordenados: \n");
        int i;
        for (i=0;i<3;i++)
            printf("%3d",listas2[i]);	//impresion de la lista con espacio de 3 lineas (%3d)
        cout<<endl;
        for (i=3;i<6;i++)
            printf("%3d",listas2[i]);
        cout<<endl;
        for (i=6;i<9;i++)
            printf("%3d",listas2[i]);

        for (i=1;i<TAM2;i++)
        {
            for (j=0;j<TAM2-1;j++)
            {
                if (listas2[j] > listas2[j+1])	 //condicion
                {
                    temp = listas2[j];	 //temp guarda momentaneamente el valor de lista[j]
                    listas2[j]=listas2[j+1];  //Asigno al la posicion lista[j], lo que hay en lista[j+1]
                    listas2[j+1]=temp;	//obtendra un nuevo valor por parte de temp.
                }}}

        printf("\n\nLos valores ORDENADOS de lista son: \n");
        for(i=0;i<3;i++)
            printf("%3d",listas2[i]);
        cout<<endl;
        for(i=3;i<6;i++)
            printf("%3d",listas2[i]);
        cout<<endl;
        for(i=6;i<9;i++)
            printf("%3d",listas2[i]);
        cout<<endl;

                        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("----------Selecciono Metodo Seleccion----------");
                        casos_seleccion();
        break;

    case 2:
        cout<<"----------Selecciono Metodo Seleccion En Estructura----------"<<endl<<endl;
        base();
        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("\tSelecciono Metodo Seleccion");
                        casos_seleccion();
    case 3:
        dificultad();
    }
}

void casos_rapido()
{
    pintarmenu2();
    moverse2();
    system("cls");

    switch(dif2)
    {
    case 0:

    cout<<"----------Selecciono Metodo Rapido En Vector----------"<<endl;

    int i;
    int tam, arreglo[10];

    /*definimos el tama�o del arreglo*/
    printf("Ingrese el tamano del arreglo: ");
    scanf("%d", &tam);
    arreglo[tam];

    /*llenamos el arreglo*/
    printf("\nIngrese valores para el arreglo:\n");
    for (i=0; i<tam; i++)
        scanf("%d", &arreglo[i]);
    printf("\n");

    /*mostramos el arreglo original*/
    printf("Arreglo Original ");
    for (i = 0; i < tam; i++)
        printf("%d ", arreglo[i]);
    printf("\n\n");

    /*hacemos el llamado a la funcion qsort
      para que ordene el arreglo*/
    qsort(arreglo, tam, sizeof(int), comparacion);

    /*mostramos el arreglo ordenado*/
    printf("Arreglo Ordenado ");
    for (i = 0; i < tam; i++)
        printf("%d ", arreglo[i]);
    printf("\n\n");

    printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("\tSelecciono Metodo Rapido");
                        casos_rapido();

    break;

    case 1:
        cout<<"----------Selecciono Metodo Rapido En Matriz----------"<<endl;


        int temp;   //Declaracion e Inicializacion de un array
        temp=0; //Variable temporal
        int j;  //variables corredoras del ciclo

        printf("\nLa lista DESORDENADA es: \n");

        for (i=0;i<TAM;i++)
            printf("%3d",listas[i]);    //impresion de la lista con espacio de 3 lineas (%3d)

        for (i=1;i<TAM;i++)
        {
            for (j=0;j<TAM-1;j++)
            {
                if (listas[j] > listas[j+1])	 //condicion
                {
                    temp = listas[j];	 //temp guarda momentaneamente el valor de lista[j]
                    listas[j]=listas[j+1];  //Asigno al la posicion lista[j], lo que hay en lista[j+1]
                    listas[j+1]=temp;	//obtendra un nuevo valor por parte de temp.
                }}}

        printf("\n\nLos valores ORDENADOS de lista son: \n");
        for(i=0;i<TAM;i++)
            printf("%3d",listas[i]);

                        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("----------Selecciono Metodo Rapido----------");
                        casos_seleccion();

break;

    case 2:
        cout<<"----------Selecciono Metodo Rapido en Estructura----------"<<endl<<endl;
        base();
        break;

    case 3:
        dificultad();
    }
}

void casos_mezclas()
{
    pintarmenu2();
    moverse2();
    system("cls");

    int vector[] = {2, 3, 5, 7, 2, 6, 1, 5, 8, 3, 2};
    switch(dif2)
    {
    case 0:
        cout<<"----------Selecciono Metodo Mezclas En Vector----------"<<endl;
        int i;
        cout << "\nLista desordenada\n";
        for(i = 0; i < 11; i++)
        cout << vector[i] << " ";
        mezcla(vector, 11);
        cout << "\n";
        cout << "\nLista ordenada\n";
        for(i=0;i<11;i++)
        cout << vector[i] << " ";
        cout << "\n";

                        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("----------Selecciono Metodo Mezclas----------");
                        casos_mezclas();

    break;

    case 1:
        cout<<"----------Selecciono Metodo Mezclas En Matriz----------"<<endl;
        cout << "Lista desordenada\n";
        for(i = 0; i < 3; i++)
            cout << vector[i] << " ";
        cout<<"\n";
        for(i = 3; i < 6; i++)
            cout << vector[i] << " ";
        cout<<"\n";
        for(i = 6; i < 9; i++)
            cout << vector[i] << " ";
        mezcla(vector, 11);
        cout << "\n";
        cout << "\nLista ordenada\n";
        for(i=0;i<3;i++)
            cout << vector[i] << " ";
        cout<<"\n";
        for(i=3;i<6;i++)
            cout << vector[i] << " ";
        cout<<"\n";
        for(i=6;i<9;i++)
            cout << vector[i] << " ";
        cout << "\n";

                        printf("\n\n");
                        system("pause");
                        system("cls");
                        printf("----------Selecciono Metodo Mezclas----------");
                        casos_mezclas();
    break;

    case 2:
        cout<<"----------Selecciono Metodo Mezclas En Estructura----------"<<endl<<endl;
        base();
        break;

    case 3:
        dificultad();
    }

}

void dificultad()
{
    pintarmenu();
    moverse();
    system("cls");

    switch(dif)
    {
        case 0:
            cout<<"Selecciono Metodo Burbuja";
            casos_burbuja();
            break;

        case 1:
            cout<<"Selecciono Metodo Sacudida";
            casos_sacudida();
            break;

        case 2:
            cout<<"Selecciono Metodo Insecion";
            casos_insercion();
            break;

        case 3:
            cout<<"Selecciono Metodo Seleccion";
            casos_seleccion();
            break;

        case 4:
            cout<<"Selecciono Metodo Rapido";
            casos_rapido();
            break;

        case 5:
            cout<<"Selecciono Metodo Mezclas";
            casos_mezclas();
            break;

        case 6:
            cout<<"Adios";
    }
}
				  
int main(){
    system("COLOR F0");
    dificultad();
}

