#include <iostream>
#include <windows.h>
#include <conio.h>
#include <String>
#include <stdio.h>

#define ARRIBA 72
#define ABAJO 80
#define ENTER 13

void dificultad();
using namespace std;

int dif=0,dif2=0;
string flecha="->";

void gotoxy(int x,int y)
{
    HANDLE hCon=GetStdHandle(STD_OUTPUT_HANDLE);
    COORD dwPos;
    dwPos.X=x;
    dwPos.Y=y;
    SetConsoleCursorPosition(hCon, dwPos);
 }

 void pintarmenu()
{

    string titulo= "BIENVENIDO A LOS METODOS DE ORDENAMIENTO";
    string OpSelect[]={"Burbuja","Sacudida","Insercion","Seleccion","Rapida","Mezclas","Salir"};

 //imprimiendo el menu
    gotoxy(5,0); cout<<titulo;

    for(int i=0; i<7; i++) //Si se aumentas lineas se debe cambiar el limite de i (tambien cambiar TODOS los numeros identicos por el nuevo
    {
        gotoxy(3,2+i); cout<<OpSelect[i];
    }
}

void pintarmenu2()
{
    string OpSelect[]={"Vector","Matriz","Estructura","Regresar"};

 //imprimiendo el menu

    for(int i=0; i<4; i++) //Si se aumentas lineas se debe cambiar el limite de i (tambien cambiar TODOS los numeros identicos por el nuevo
    {
        gotoxy(3,2+i); cout<<OpSelect[i];
    }
}

//imprimir movimiento de la flecha
int moverse()
{
    gotoxy(1,2+dif); cout<<flecha;

    if(kbhit())
    {
        char tecla=getch();

        if(tecla==ABAJO)
        {
            dif++;

            if(dif<7) //Este maneja a donde vuelve a aparecer la flecha (osea en la primer linea)
            {
                gotoxy(1, 2+dif-1); cout<<"  ";
            }
            else
            {
                gotoxy(1, 2+dif-1); cout<<"  ";
                dif=0;
            }
        }
    if(tecla==ARRIBA)
    {
        dif--;

        if(dif<0)
        {
            gotoxy(1, 2+dif+1); cout<<"  ";
            dif=6; //Este maneja a donde vuelve a aparecer la flecha (osea que aparezca en la ultima linea)
        }
        else
        {
        gotoxy(1, 2+dif+1); cout<<"  ";
        }
    }
   if(tecla==ENTER)
   return dif;
   }
 Sleep(50);
 moverse();
}

int moverse2()
{
    gotoxy(1,2+dif2); cout<<flecha;

    if(kbhit())
    {
        char tecla=getch();

        if(tecla==ABAJO)
        {
            dif2++;

            if(dif2<4) //Este maneja a donde vuelve a aparecer la flecha (osea en la primer linea)
            {
                gotoxy(1, 2+dif2-1); cout<<"  ";
            }
            else
            {
                gotoxy(1, 2+dif2-1); cout<<"  ";
                dif2=0;
            }
        }
    if(tecla==ARRIBA)
    {
        dif2--;

        if(dif2<0)
        {
            gotoxy(1, 2+dif2+1); cout<<"  ";
            dif2=3; //Este maneja a donde vuelve a aparecer la flecha (osea que aparezca en la ultima linea)
        }
        else
        {
        gotoxy(1, 2+dif2+1); cout<<"  ";
        }
    }
   if(tecla==ENTER)
   return dif2;
   }
 Sleep(50);
 moverse2();
}

void casos_burbuja()
{
    pintarmenu2();
    moverse2();
    system("cls");

    switch(dif2)
                {
                    case 0:
                        cout<<"Selecciono Modo Vector\n\nIngrese Sus Datos a Ordenar\n";
                        int array[10];
                        int i,j,aux,a,lim;

                        printf("Digite La Cantidad de Valores Que Desee Ordenar: ");
                        scanf("%i",&lim);

                        printf("\n");
                        for(i=0;i<lim;i++)
                        {
                            printf("Valor en [%i]: ",i+1);
                            scanf("%i",&array[i]);
                        }

                        for(i=0;i<lim;i++)
                        {
                            for(j=0;j<lim;j++)
                        {

                            if(array[j] > array[j+1])
                            {
                                aux = array[j];
                                array[j] = array[j+1];
                                array[j+1] = aux;
                            }
                        }
                        }

                        printf("\n");
                        //Ascendente
                        for(i=0;i<lim;i++)
                        {
                            printf("%i ",array[i]);
                        }
                        printf("\n\n");

                        //Desendente
                        for(i=lim-1;i>=0;i--)
                        {
                            printf("%i ",array[i]);
                        }
                        break;

                    case 1:
                        cout<<"Selecciono Modo Matriz\n\nIngrese Sus Datos a Ordenar:\n";
                        int f,c,mayor;
                        int matriz[10][10];

                        printf("Cual es el numero de filas y columnas de su matriz?: ");
                        scanf("%i",&lim);

                        for(i=0;i<lim;i++)
                        {
                            for(j=0;j<lim;j++)
                            {
                                printf("Valor en [%i][%i]: ",i+1,j+1);
                                scanf("%i,%i",&matriz[i][j]);
                            }
                        }

                        //ordeno la matriz de mayor a menor
                        for(int i=0; i<lim; i++)
                        {
                            for(int j=0; j<lim; j++)
                            {
                                for(int x=0; x<lim;x++)
                                {
                                    for(int y=0; y<lim; y++)
                                    {
                                        if(matriz[i][j]>matriz[x][y])
                                        {
                                            mayor=matriz[i][j];
                                            matriz[i][j]=matriz[x][y];
                                            matriz[x][y]=mayor;
                                        }}}}}

                    //imprimo la matriz como ordenada
                    for(int i=0; i<lim; i++)
                    {
                        for(int j=0; j<lim; j++)
                        {
                            printf("%i ",matriz[i][j]);
                        }
                                printf("\n");
                    }
                        break;

                    case 2:
                        cout<<"Selecciono Modo Estructura\n\nEsta Funcion Sera Agregada Proximamente...";
                        break;

                    case 3:
                        dificultad();
                        break;
                }
}

void casos_sacudida()
{
    pintarmenu2();
    moverse2();
    system("cls");

    switch(dif2)
    {
    case 0:
        cout<<"Selecciono Metodo Sacudida en Vector\n";

        break;

    case 1:
        cout<<"Selecciono Metodo Sacudida en Matriz\n";
        break;

    case 2:
        cout<<"Selecciono Metodo Sacudida en Estructura\n";
        break;
    }
}

void dificultad()
{
    pintarmenu();
    moverse();
    system("cls");

    switch(dif)
    {
        case 0:
            cout<<"Selecciono Metodo Burbuja";
            casos_burbuja();
            break;

        case 1:
            cout<<"Selecciono Metodo Sacudida";
            casos_sacudida();
            break;

        case 2:
            cout<<"Selecciono Metodo Insecion";
            break;

        case 3:
            cout<<"Selecciono Metodo Seleccion";
            break;

        case 4:
            cout<<"Selecciono Metodo Rapido";
            break;

        case 5:
            cout<<"Selecciono Metodo Mezclas";
            break;

        case 6:
            cout<<"Adios. Perra";
    }
}

int main(){

dificultad();

}

